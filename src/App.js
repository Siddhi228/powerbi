import { PowerBIEmbed } from "powerbi-client-react";
import { models } from "powerbi-client";

function App() {
  const token =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyIsImtpZCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvZTQ1NzVkZGYtNTExMi00NjJhLWFlY2YtZDJjNDViMjQxYzg0LyIsImlhdCI6MTYyMjc5ODY1MCwibmJmIjoxNjIyNzk4NjUwLCJleHAiOjE2MjI4MDI1NTAsImFjY3QiOjAsImFjciI6IjEiLCJhaW8iOiJBU1FBMi84VEFBQUFjdmswNEkxOVVodnQrTGV5bkp4Mm5HTGh2SlJkNGJJc2kwa3RIOWVrbGhFPSIsImFtciI6WyJwd2QiLCJy";

  return (
    <PowerBIEmbed
      embedConfig={{
        type: "report", // Supported types: report, dashboard, tile, visual and qna
        id: "21b33c0a-5b86-44a0-8623-5ebaff1c96b3",
        embedUrl:
          "https://app.powerbi.com/reportEmbed?reportId=21b33c0a-5b86-44a0-8623-5ebaff1c96b3&groupId=0cf4202f-6cf7-4e23-8b26-97fe6130948b&w=2&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly9XQUJJLUlORElBLUNFTlRSQUwtQS1QUklNQVJZLXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0IiwiZW1iZWRGZWF0dXJlcyI6eyJtb2Rlcm5FbWJlZCI6dHJ1ZSwiY2VydGlmaWVkVGVsZW1ldHJ5RW1iZWQiOnRydWV9fQ%3d%3d",
        accessToken: token,
        tokenType: models.TokenType.Aad,
        settings: {
          panes: {
            filters: {
              expanded: false,
              visible: false,
            },
          },
          background: models.BackgroundType.Transparent,
        },
      }}
      eventHandlers={
        new Map([
          [
            "loaded",
            function () {
              console.log("Report loaded");
            },
          ],
          [
            "rendered",
            function () {
              console.log("Report rendered");
            },
          ],
          [
            "error",
            function (event) {
              console.log(event.detail);
            },
          ],
        ])
      }
      cssClassName={"report-style-class"}
      getEmbeddedComponent={(embeddedReport) => {
        window.report = embeddedReport;
      }}
    />
  );
}
export default App;
